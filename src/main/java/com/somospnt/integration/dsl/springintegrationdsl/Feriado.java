package com.somospnt.integration.dsl.springintegrationdsl;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Feriado {

    private String descripcion;
    private String tipo;
    private String fecha;


}
