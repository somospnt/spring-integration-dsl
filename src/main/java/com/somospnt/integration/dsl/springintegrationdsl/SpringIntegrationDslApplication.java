package com.somospnt.integration.dsl.springintegrationdsl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.integration.config.EnableIntegrationManagement;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.http.config.EnableIntegrationGraphController;
import org.springframework.integration.http.dsl.Http;
import org.springframework.integration.json.ObjectToJsonTransformer;

@SpringBootApplication
@EnableIntegrationGraphController(allowedOrigins = "*")
@EnableIntegrationManagement
public class SpringIntegrationDslApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringIntegrationDslApplication.class, args);
    }

    @Bean
    public IntegrationFlow proxyApidayFlow() {
        return IntegrationFlows.from(Http.inboundGateway("/apiday/proxy/{anio}")
                .requestMapping(r -> r
                .methods(HttpMethod.GET))
                .payloadExpression("#pathVariables.anio"))
                .channel("apiDay")
                .get();
    }

    @Bean
    public IntegrationFlow apiDayFlow() {
        return IntegrationFlows.from("apiDay")
                .handle(Http.outboundGateway("http://apiday.somospnt.com/api/feriados/{anio}")
                        .uriVariable("anio", "payload")
                        .charset("UTF-8")
                        .httpMethod(HttpMethod.GET)
                        .expectedResponseType(Feriado[].class)
                )
                .split()
                .channel("routerChannel")
                .get();
    }

    @Bean
    public IntegrationFlow routerFlow() {
        return IntegrationFlows.from("routerChannel")
                .<Feriado, Boolean>route(p -> p.getTipo().equals("INAMOVIBLE"),
                        m -> m.channelMapping(true, "aggregateChannel")
                                .channelMapping(false, "errorChannel"))
                .get();
    }

    @Bean
    public IntegrationFlow aggregateFlow() {
        return IntegrationFlows.from("aggregateChannel")
                .log(m -> m.getPayload())
                .aggregate(r -> r.releaseStrategy(g -> g.size() > 8))
                .transform(new ObjectToJsonTransformer())
                .get();
    }

}
